import React from "react";
import "../../style-general.css";
import "./App.css";
import Header from "../Header/Header";
import Footer from "../Footer/Footer";
import Homepage from "../Pages/Homepage/Homepage";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";

function App() {
  return (
    <Router>
      <div className="App">
        <Header />
        <div className="content">
          <Switch>
            <Route path="/">
              <Homepage />
            </Route>
            <Route path="/vende">

            </Route>
          </Switch>
        </div>
        <Footer/>
      </div>
    </Router>
  );
}

export default App;
