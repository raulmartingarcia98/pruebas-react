import "./Header.css";
import logo from "../../img/logo.png";
import { Link } from "react-router-dom";
function Header() {
  return (
    <header>
      <Link to="/">
        <img src={logo} alt="Valabra" />
      </Link>
      <nav class="header-menu">
        <ul>
          <li>
            <Link to="/" className="activa">
              Home
            </Link>
          </li>
          <li>
            <Link to="/vende">Vende</Link>
          </li>
          <li>
            <Link to="compra">Compra</Link>
          </li>
          <li>
            <Link to="/quienes-somos">Quiénes somos</Link>
          </li>
          <li>
            <Link to="/contactanos">Contáctanos</Link>
          </li>
        </ul>
      </nav>
    </header>
  );
}

export default Header;
